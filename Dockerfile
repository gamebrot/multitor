FROM alpine:latest

ENV BUILD_PACKAGES="build-base openssl" \
    PACKAGES="tor tsocks torsocks sudo net-tools bash git haproxy privoxy npm procps nano bash curl wget ca-certificates"

RUN \
apk update && apk add --no-cache $BUILD_PACKAGES $PACKAGES && \
update-ca-certificates && \
npm install -g http-proxy-to-socks

RUN \
wget https://github.com/jech/polipo/archive/master.zip -O polipo.zip && \
unzip polipo.zip && \
cd polipo-master && \
make V=1 && \
install polipo /usr/bin/ && \
cd .. && \
rm -rfv polipo.zip polipo-master && \
mkdir -pv /usr/share/polipo/www /var/cache/polipo 

RUN git clone https://gitlab.com/gamebrot/multitor && \
cd multitor && \
./setup.sh install && \
mkdir -pv /var/log/multitor/privoxy/ && \
mkdir -pv /var/log/polipo/ && \
sed -i s/127.0.0.1:16379/0.0.0.0:16379/g templates/haproxy-template.cfg && \
sed -i s/127.0.0.1:16380/0.0.0.0:16380/g templates/haproxy-template.cfg

RUN apk del $BUILD_PACKAGES && apk cache clean --purge

COPY startup.sh /multitor/
RUN chmod +x /multitor/startup.sh
WORKDIR /multitor/
EXPOSE	16379 16380

CMD ["./startup.sh"]
